[![pipeline status](https://gitlab.com/crapinheiro/curso-cypress-basico/badges/Curso-Cypress-Basico/pipeline.svg)](https://gitlab.com/crapinheiro/curso-cypress-basico/-/commits/Curso-Cypress-Basico)

# curso-cypress-basico

Arquivos do curso básico de testes de Frontend usando Cypress

    Para instalar o Cypress, utilize o comando:

> npm install cypress --dev

Isso vai fazer com q o Cypress seja instalado como uma dependência de desenvolvimento do projeto.


    Para rodar os arquivos usando o Cypress usando um navegador, utilize o comando:

> npx cypress open

Será criado basicamente um arquivo chamado cypress.json e uma pasta Cypress, com os diretórios 
- fixtures
- integration (onde estarão nossos testes de exemplo e arquivos de teste)
- plugins
- support

## Comandos Básicos

    it('Passo',() => {})
Cria um passo de teste
> **it(**'Preenche campos do tipo texto', **() => {**
> 
>  //get: localiza elementos através de 'selectors' CSS (id)
>  //type: digita um texto numa caixa de texto
>  const firstName = "Claudio";
>  const lastName = "Pinheiro"
>  cy.get('#first-name').type(firstName);
>  cy.get('#last-name').type(lastName);
> **});**


    cy.get(<selector>) 
identifica elementos através de selectors CSS

    cy.get(<selector>).type('string')
digita uma string em um campo texto




